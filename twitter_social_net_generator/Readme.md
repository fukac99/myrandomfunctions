## Usage

1. Change your credentials in the R script
2. setwd("<your directory>") #set your directory
3. source("./generate_social_graph.R")
4. get_graph_for_user("<twitter_handle>", save_dir <- "<directory to save graph object>", level=1)

## Example
get_graph_for_user("knoyd_com", save_dir <- "~/Documents/")

Creates social graph of followers and followers of followers for twitter user "knoyd_com" (http://knoyd.com) and saves to Documents
