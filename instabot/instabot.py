#!/usr/bin/env python
# -*- coding: utf-8 -*-
from InstagramAPI import InstagramAPI
import requests
import random
import time
import datetime
import logging
import itertools
from subprocess import call


class InstaBot:
    """
    Instagram bot v 1.0
    like_per_day=1000 - How many likes set bot in one day.

    media_max_like=0 - Don't like media (photo or video) if it have more than
    media_max_like likes.

    media_min_like=0 - Don't like media (photo or video) if it have less than
    media_min_like likes.

    tag_list = ['cat', 'car', 'dog'] - Tag list to like.

    max_like_for_one_tag=5 - Like 1 to max_like_for_one_tag times by row.

    log_mod = 0 - Log mod: log_mod = 0 log to console, log_mod = 1 log to file,
    log_mod = 2 no log.

    """

    # If instagram ban you - query return 400 error.
    error_400 = 0
    # If you have 3 400 error in row - looks like you banned.
    error_400_to_ban = 3
    # If InstaBot think you are banned - going to sleep.
    ban_sleep_time = 2 * 60 * 60

    # All counter.
    like_counter = 0
    follow_counter = 0
    unfollow_counter = 0
    unfollowOld_counter = 0
    comments_counter = 0

    # List of user_id, that bot follow
    bot_follow_list = []

    # Log setting.
    log_file_path = ''
    log_file = 0

    # Other.
    user_id = 0
    media_by_tag = 0
    login_status = False

    # For new_auto_mod
    next_iteration = {"Like": 0, "Follow": 0, "Unfollow": 0, "Comments": 0, "UnfollowOld":0}

    def __init__(self, login, password,
                 like_per_day=1000,
                 media_max_like=50,
                 media_min_like=10,
                 follow_per_day=0,
                 follow_time=5 * 60 * 60,
                 unfollow_per_day=0,
                 comments_per_day=0,
                 tag_list=['cat', 'car', 'dog'],
                 max_like_for_one_tag=5,
                 unfollow_break_min=15,
                 unfollow_break_max=30,
                 log_mod=0,
                 proxy="",
                 follow_file="./already_followed.txt",
                 insta_follow_file="./accounts_to_follow.txt",
                 follow_by="hashtag"):
                     
        self.api = InstagramAPI(username=login, password=password)
        self.api.login()
        
        self.follow_by = follow_by

        self.bot_start = datetime.datetime.now()
        self.unfollow_break_min = unfollow_break_min
        self.unfollow_break_max = unfollow_break_max
        self.time_in_day = 24 * 60 * 60
        self.file_path = follow_file
        
        # Like
        self.like_per_day = like_per_day
        if self.like_per_day != 0:
            self.like_delay = self.time_in_day / self.like_per_day

        # Follow
        self.follow_time = follow_time
        self.follow_per_day = follow_per_day
        if self.follow_per_day != 0:
            self.follow_delay = self.time_in_day / self.follow_per_day

        # Unfollow
        self.unfollow_per_day = unfollow_per_day
        if self.unfollow_per_day != 0:
            self.unfollow_delay = self.time_in_day / self.unfollow_per_day

        # Comment
        self.comments_per_day = comments_per_day
        if self.comments_per_day != 0:
            self.comments_delay = self.time_in_day / self.comments_per_day

        # Don't like if media have more than n likes.
        self.media_max_like = media_max_like
        # Don't like if media have less than n likes.
        self.media_min_like = media_min_like
        # Auto mod seting:
        # Default list of tag.
        self.tag_list = tag_list
        # Get random tag, from tag_list, and like (1 to n) times.
        self.max_like_for_one_tag = max_like_for_one_tag
        # log_mod 0 to console, 1 to file
        self.log_mod = log_mod
        self.s = requests.Session()

        self.media_by_tag = []

        now_time = datetime.datetime.now()
        log_string = 'Instabot v1.0.1 started at %s:\n' % \
                     (now_time.strftime("%d.%m.%Y %H:%M"))
        self.write_log(log_string)
        
        # Follow by account
        fp = open(insta_follow_file, "rb")

        self.accounts_to_follow = []
        for acc in fp.readlines():
            self.api.searchUsername(acc.strip())
			
            if self.api.LastJson["status"] == "ok":
                this_acc = self.api.LastJson["user"]["pk"]
                self.accounts_to_follow.append(this_acc)
            else:
                self.write_log("Failed to pull accounts")
			
            self.this_account = self.accounts_to_follow[0]
            self.this_next_max_id = ''
            self.ids_to_follow = []
        
    def auto_follow_by_account(self):
        if time.time() > self.next_iteration["Like"] and self.follow_per_day != 0:
            if len(self.ids_to_follow) == 0:
                self.write_log("Pulling new followers for user {}".format(self.this_account))
                self.api.getUserFollowers(self.this_account, self.this_next_max_id)
                temp = self.api.LastJson
                for item in temp["users"]:
                    self.ids_to_follow.append(item["pk"])
                self.write_log("Pulled {} followers".format(len(self.ids_to_follow)))
                
                if temp["big_list"] == False:
                    self.accounts_to_follow.remove(self.this_account)
                    self.this_account = self.accounts_to_follow[0]
                    self.this_next_max_id = ''
                else:
                    self.this_next_max_id = temp["next_max_id"]
            try:
                with open(self.file_path, "r") as fp:
                    already_followed = fp.readline().split(",")
            except:
                call(["touch", self.file_path])
                with open(self.file_path, "r") as fp:
                    already_followed = fp.readline().split(",")
                	
            for md in self.ids_to_follow:
                if md == self.user_id:
                    self.write_log("Keep calm - It's your own profile ;)")
                    continue
                if not str(md) in already_followed:
                    log_string = "Trying to follow: %s" % (md)
                    self.write_log(log_string)
                	
                    if self.api.follow(md) != False:
                        self.bot_follow_list.append([md,
                                                 time.time()])
                        self.ids_to_follow.remove(md)
                    else:
                        self.ids_to_follow.remove(md)
                        continue
                    self.next_iteration["Follow"] = time.time() + \
                                                    self.add_time(self.follow_delay)
                    self.follow_counter += 1
                    already_followed.append(str(md))
                    with open(self.file_path, "w") as fp:
                        fp.write(",".join([str(ch) for ch in already_followed]))
                    log_string = "Followed: %s. Follow #%i." % \
                                     (md,
                                      self.follow_counter)
                    self.write_log(log_string)
                    break        	
                    	       	

    def get_media_id_by_tag(self, tag):
        """ Get media ID set, by your hashtag """
        log_string = "Get media id by tag: %s" % (tag)
        self.write_log(log_string)
        
        self.api.getHashtagFeed(tag)
        if self.api.LastResponse.status_code == 200:
            self.media_by_tag = self.api.LastJson["items"]
        else:
            self.media_by_tag = []
            self.write_log("Except on get_media!")
            
    def get_media_id_by_username(self, user_id):
        """ Get media ID set, by user_id """
        log_string = "Get media id by username: %s" % (user_id)
        self.write_log(log_string)
        
        self.api.getUserFeed(user_id)
        if self.api.LastResponse.status_code == 200:
            self.media_by_tag = self.api.LastJson["items"][:5]
        else:
            self.media_by_tag = []
            self.write_log("Except on get_media!")
            
    def get_user_followers(self):
        max_id = ""
        folls = []
        cont = True
        while cont:
            self.api.getUserFollowers(self.api.username_id, maxid = max_id)
            if self.api.LastResponse.status_code == 200:
                for us in self.api.LastJson["users"]:
                    folls.append(us["pk"])
                if "next_max_id" in self.api.LastJson:
                    max_id = self.api.LastJson["next_max_id"]
                else:
                    cont = False
        return folls
    
    def get_user_followings(self):
        max_id = ""
        folls = []
        cont = True
        while cont:
            self.api.getUserFollowings(self.api.username_id, maxid = max_id)
            if self.api.LastResponse.status_code == 200:
                for us in self.api.LastJson["users"]:
                    folls.append(us["pk"])
                if "next_max_id" in self.api.LastJson:
                    max_id = self.api.LastJson["next_max_id"]
                else:
                    cont = False
        return folls

    def like_all_exist_media(self, media_size=-1, delay=True):
        """ Like all media ID that have self.media_by_tag """
        if len(self.media_by_tag) != 0:
            i = 0
            for d in self.media_by_tag:
            	#print "here"
                #Media count by this tag
                if media_size > 0 or media_size < 0:
                    media_size -= 1
					
                    #print "here1"
                    try:
                        l_c = len(self.media_by_tag[i]["likers"])
                    except KeyError:
                        l_c = 0
            		  #print l_c
                    if ((l_c <= self.media_max_like and l_c >= self.media_min_like)
                        or (self.media_max_like == 0 and l_c >= self.media_min_like)
                        or (self.media_min_like == 0 and l_c <= self.media_max_like)
                        or (self.media_min_like == 0 and self.media_max_like == 0)):
                        #print "here3"
                        if (self.media_by_tag[i]['user']['pk'] == self.user_id):
                        	
                        	self.write_log("Keep calm - It's your own media ;)")
                        	return False
                        log_string = "Trying to like media: %s" % \
                                     (self.media_by_tag[i]['id'])
                        self.write_log(log_string)
                        #print "here4"
                        self.api.like(self.media_by_tag[i]['id'])
                        #print "here5"
                        if self.api.LastResponse.status_code == 200:
                            # Like, all ok!
                            self.error_400 = 0
                            self.like_counter += 1
                            #print "here6"
                            log_string = "Liked: %s. Like #%i." % \
                                         (self.media_by_tag[i]['id'],
                                          self.like_counter)
                            self.write_log(log_string)
                        elif self.api.LastResponse.status_code == 400:
                            log_string = "Not liked: %i" \
                                         % (self.api.LastResponse.status_code)
                            self.write_log(log_string)
                            # Some error. If repeated - can be ban!
                            if self.error_400 >= self.error_400_to_ban:
                                # Look like you banned!
                                time.sleep(self.ban_sleep_time)
                            else:
                                self.error_400 += 1
                        else:
                            log_string = "Not liked: %i" \
                                         % (self.api.LastResponse.status_code)
                            self.write_log(log_string)
                            return False
                            # Some error.
                        i += 1
                        if delay:
                            time.sleep(self.like_delay * 0.9 +
                                       self.like_delay * 0.2 * random.random())
                        else:
                            return True
                    else:
                        continue
                else:
                    return False
        else:
            self.write_log("No media to like!")

    def new_auto_mod(self):
        while True:
            # ------------------- Get media_id -------------------
            if len(self.media_by_tag) == 0:
                if self.follow_by == "hashtag":
                    self.get_media_id_by_tag(random.choice(self.tag_list))
                    self.this_tag_like_count = 0
                    self.max_tag_like_count = random.randint(1, self.max_like_for_one_tag)
                elif self.follow_by == "account":
                    self.get_media_id_by_username(random.choice(self.accounts_to_follow))
            # ------------------- Like -------------------
            self.new_auto_mod_like()
            # ------------------- Follow -------------------
            if self.follow_by == "hashtag":
                self.new_auto_mod_follow()
            elif self.follow_by == "account":
                self.auto_follow_by_account()
            # ------------------- Unfollow -------------------
            self.new_auto_mod_unfollow()
            # ------------------- Unfollow_old -------------------
            self.new_auto_mod_unfollowold()
            # ------------------- Comment -------------------
            self.new_auto_mod_comments()

            # Bot iteration in 30 sec
            time.sleep(30)
            # print("Tic!")

    def new_auto_mod_like(self):
        if time.time() > self.next_iteration["Like"] and self.like_per_day != 0 \
                and len(self.media_by_tag) > 0:
            # You have media_id to like:
            if self.like_all_exist_media(media_size=1, delay=False):
                # If like go to sleep:
                self.next_iteration["Like"] = time.time() + \
                                              self.add_time(self.like_delay)
                # Count this tag likes:
                self.this_tag_like_count += 1
                if self.this_tag_like_count >= self.max_tag_like_count:
                    self.media_by_tag = []
            # Del first media_id
            del self.media_by_tag[0]

    def new_auto_mod_follow(self):
        if time.time() > self.next_iteration["Follow"] and \
                        self.follow_per_day != 0 and len(self.media_by_tag) > 0:
            try:
                with open(self.file_path, "r") as fp:
                    already_followed = fp.readline().split(",")
            except:
                call(["touch", self.file_path])
                with open(self.file_path, "r") as fp:
                    already_followed = fp.readline().split(",")
            
            for md in self.media_by_tag:
                if md["user"]["pk"] == self.user_id:
                    self.write_log("Keep calm - It's your own profile ;)")
                    continue
                if not str(md["user"]["pk"]) in already_followed:
                    log_string = "Trying to follow: %s" % (md["user"]["pk"])
                    self.write_log(log_string)
    
                    if self.api.follow(md["user"]["pk"]) != False:
                        self.bot_follow_list.append([md["user"]["pk"],
                                                     time.time()])
                        self.next_iteration["Follow"] = time.time() + \
                                                        self.add_time(self.follow_delay)
                        self.follow_counter += 1
                        already_followed.append(str(md["user"]["pk"]))
                        with open(self.file_path, "w") as fp:
                            fp.write(",".join([str(ch) for ch in already_followed]))
                        log_string = "Followed: %s. Follow #%i." % \
                                         (md["user"]["pk"],
                                          self.follow_counter)
                        self.write_log(log_string)
                    break
                        

    def new_auto_mod_unfollow(self):
        if time.time() > self.next_iteration["Unfollow"] and \
                        self.unfollow_per_day != 0 and len(self.bot_follow_list) > 0:
            #curr_followers = self.get_user_followers()
            random.shuffle(self.bot_follow_list)
            for f in self.bot_follow_list:
                if time.time() > (f[1] + self.follow_time):
                    if True: #not f[0] in curr_followers:
                        log_string = "Trying to unfollow: %s" % (f[0])
                        self.write_log(log_string)
    
                        if self.api.unfollow(f[0]) != False:
                            self.bot_follow_list.remove(f)
                            self.unfollow_counter += 1
                            self.next_iteration["Unfollow"] = time.time() + \
                                                              self.add_time(self.unfollow_delay)
                            log_string = "Unfollowed: %s. Unfollow #%i." % \
                                         (f[0],
                                          self.unfollow_counter)
                            self.write_log(log_string)
                        break
                                                          
    def new_auto_mod_unfollowold(self):
        if time.time() > self.next_iteration["UnfollowOld"] and \
                        self.unfollow_per_day != 0:
            followers = self.get_user_followers()
            following = self.get_user_followings()
            candidates = list(set(following).difference(set(followers)))
            
            bot_follow = [x[0] for x in self.bot_follow_list]
            
            for f in candidates:
                if not f in bot_follow:
                    log_string = "Trying to unfollow old: %s" % (f)
                    self.write_log(log_string)
    
                    if self.api.unfollow(f) != False:
                        self.next_iteration["UnfollowOld"] = time.time() + \
                                                          self.add_time(self.unfollow_delay)
                        self.unfollowOld_counter += 1
                        log_string = "UnfollowedOld: %s. UnfollowOld #%i." % \
                                         (f,
                                          self.unfollowOld_counter)
                        self.write_log(log_string)
                    break
            
     #TODO not working with new api
    def new_auto_mod_comments(self):
        pass
#        if time.time() > self.next_iteration["Comments"] and self.comments_per_day != 0 \
#                and len(self.media_by_tag) > 0 \
#                and self.check_exisiting_comment(self.media_by_tag[0]['code']) == False:
#            comment_text = self.generate_comment()
#            log_string = "Trying to comment: %s" % (self.media_by_tag[0]['id'])
#            self.write_log(log_string)
#            if self.comment(self.media_by_tag[0]['id'], comment_text) != False:
#                self.next_iteration["Comments"] = time.time() + \
#                                                  self.add_time(self.comments_delay)

    def add_time(self, time):
        """ Make some random for next iteration"""
        return time * 0.9 + time * 0.2 * random.random()

    def generate_comment(self):
        c_list = list(itertools.product(
            ["this", "the", "your"],
            ["photo", "picture", "pic", "shot", "snapshot"],
            ["is", "looks", "feels", "is really"],
            ["great", "super", "good", "very good",
             "good", "wow", "WOW", "cool",
             "GREAT", "magnificent", "magical", "very cool",
             "stylish", "so stylish", "beautiful",
             "so beautiful", "so stylish", "so professional",
             "lovely", "so lovely", "very lovely",
             "glorious", "so glorious", "very glorious",
             "adorable", "excellent", "amazing"],
            [".", "..", "...", "!", "!!", "!!!"]))

        repl = [("  ", " "), (" .", "."), (" !", "!")]
        res = " ".join(random.choice(c_list))
        for s, r in repl:
            res = res.replace(s, r)
        return res.capitalize()
    
    #TODO not working with new api
#    def check_exisiting_comment(self, media_code):
#        url_check = self.url_media_detail % (media_code)
#        check_comment = self.s.get(url_check)
#        all_data = json.loads(check_comment.text)
#        if all_data['media']['owner']['id'] == self.user_id:
#                self.write_log("Keep calm - It's your own media ;)")
#                # Del media to don't loop on it
#                del self.media_by_tag[0]
#                return True
#        comment_list = list(all_data['media']['comments']['nodes'])
#        for d in comment_list:
#            if d['user']['id'] == self.user_id:
#                self.write_log("Keep calm - Media already commented ;)")
#                # Del media to don't loop on it
#                del self.media_by_tag[0]
#                return True
#        return False

    def write_log(self, log_text):
        """ Write log by print() or logger """

        if self.log_mod == 0:
            try:
                print(log_text)
            except UnicodeEncodeError:
                print("Your text has unicode problem!")
        elif self.log_mod == 1:
            # Create log_file if not exist.
            if self.log_file == 0:
                self.log_file = 1
                now_time = datetime.datetime.now()
                self.log_full_path = '%s%s_%s.log' % (self.log_file_path,
                                                      self.api.username,
                                                      now_time.strftime("%d.%m.%Y_%H:%M"))
                formatter = logging.Formatter('%(asctime)s - %(name)s '
                                              '- %(message)s')
                self.logger = logging.getLogger(self.api.username)
                self.hdrl = logging.FileHandler(self.log_full_path, mode='w')
                self.hdrl.setFormatter(formatter)
                self.logger.setLevel(level=logging.INFO)
                self.logger.addHandler(self.hdrl)
            # Log to log file.
            try:
                self.logger.info(log_text)
            except UnicodeEncodeError:
                print("Your text has unicode problem!")
