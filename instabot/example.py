#!/usr/bin/env python
# -*- coding: utf-8 -*-

from instabot import InstaBot
import time

bot = InstaBot(login="TwoSundowners", password="jamjams",
               like_per_day=1000,
               comments_per_day=0,
               tag_list=['nomad', 'travel', 'mountains', 'adventure', 'nature', 'climbing', 'travelgram', 'adventurelife', 'exploring', 'travellife', 'wanderlust'],
               max_like_for_one_tag=100,
	       media_min_like=1,
               follow_per_day=1000,
               follow_time=10*60*60,
               unfollow_per_day=1000,
               unfollow_break_min=15,
               unfollow_break_max=60,
               log_mod=0,
	       follow_by="account")

while True:
    try:
        bot.new_auto_mod()
    except:
        time.sleep(120)
        
