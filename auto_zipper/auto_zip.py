
# coding: utf-8

# In[1]:

import os
import zipfile
import re


# In[2]:

def should_zip(folder):
    files = [x.lower() for x in os.listdir(folder)]
    if (any('.jpeg' in s for s in files) or 
       any('.jpg' in s for s in files) or 
       any('.cr2' in s for s in files) or 
       any('.xmp' in s for s in files) or 
       any('.mov' in s for s in files) or
       any('.thm' in s for s in files) or 
       any('.psd' in s for s in files)):
        return True
    else:
        return False
        


# In[ ]:

def explore_folder(folder, level=0, name_file='', original_folder='./'):
    files = os.listdir(folder)
    for f in files:
        if os.path.isdir(folder + f):
            print '{}Exploring: {}'.format(' '*4*level, f)
            if should_zip(folder + f):
                name = name_file + '_' + re.sub('[^0-9a-zA-Z]+', '', f.replace(' ', ''))
                print '    {}Creating zipfile: {}'.format(' '*4*level, name)
                
                with zipfile.ZipFile(original_folder + name + '.zip',
                                     "w",
                                     zipfile.ZIP_DEFLATED,
                                     allowZip64=True) as zf:
                    for root, _, filenames in os.walk(os.path.basename(original_folder + name)):
                        for n1 in filenames:
                            nm = os.path.join(root, n1)
                            nm = os.path.normpath(nm)
                            zf.write(nm, nm)
                
            else:
                l = level + 1
                n = name_file + '_' + re.sub('[^0-9a-zA-Z]+', '', f.replace(' ', ''))
                print '    {}Explore further: {}'.format(' '*4*level, folder + f)
                explore_folder(folder + f + '/', level=l, name_file=n)


# In[ ]:

if __name__ == '__main__':
    folder = raw_input('Full path of folder to zip:')
    # if ends with '/'
    if folder[-1] == '/':
        base_name = folder.split('/')[-2]
    else:
        base_name = folder.split('/')[-1]
        
    print 'Zipping all subfolders of: {}'.format(folder)
    explore_folder(folder, name_file=base_name)


# In[ ]:



